﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader
{
    public class CoR_Object : Delegates
    {
        private String name;
        private CoR_Object nextObjectInChain;
        private CoR_Object previousObjectInChain;
        public event CoR_Delegate SelectedObject;


        public CoR_Object(String filename)
        {
            this._name = filename;
        }

        public CoR_Object(String filename, CoR_Object firstObjectInChain)
        {
            this._name = filename;
            this.nextObjectInChain = firstObjectInChain;
        }

        public void HandleRequest(String filename)
        {
            if (string.Compare(filename, this._name) != 0)  // !=0 -> Strings dont match
            {
                ProceedWithNextObject(filename);
            }
            else
                SelectedObject.Invoke(this);
        }

        private void ProceedWithNextObject(String filename)
        {
            _nextObjectInChain.HandleRequest(filename);
        }

        private void ProceedWithPreviousObject(String filename)
        {
            _previousObjectInChain.HandleRequest(filename);
        }

        public string _name
        {
            get { return name; }
            set { name = value; }
        }

        public CoR_Object _nextObjectInChain
        {
            get { return nextObjectInChain; }
            set { nextObjectInChain = value; }
        }

        public CoR_Object _previousObjectInChain
        {
            get { return previousObjectInChain; }
            set { previousObjectInChain = value; }
        }

    }

    public class CoR_Composite
    {
        private List<CoR_Object> chainlink = new List<CoR_Object>();
        private int indexForSelectedObject = 0;
        private CoR_Object activeObject = null;

        public void CreateNewObjectInChain(String filename)
        {
            CoR_Object tempObject = new CoR_Object(filename);
            
            // If object is the one searching for, fire delegate as you cannot use a return value since this stops for every itteration.
            tempObject.SelectedObject += new CoR_Delegate(this.UpdateActiveObject);

            // Add connections between the objects in the chain. 
            if (chainlink.Count() > 0)
            {
                chainlink[chainlink.Count() - 1]._nextObjectInChain = tempObject;
                tempObject._previousObjectInChain = chainlink[chainlink.Count() - 1];
            }
            else
                activeObject = tempObject;

            AddObjectToChain(tempObject);
            ConnectLastObjectInChainToFirst();
        }

        private void ConnectLastObjectInChainToFirst()
        {
            chainlink[chainlink.Count() - 1]._nextObjectInChain = chainlink[0];
            chainlink[0]._previousObjectInChain = chainlink[chainlink.Count() - 1];
        }


        public void UpdateActiveObject(CoR_Object activeobject)        // Called by an event, delegate
        {
            this.activeObject = activeobject;
        }

        public void StepForwardInChain()
        {
            activeObject = activeObject._nextObjectInChain;
        }

        public bool ImagesInChain()
        {
            if (chainlink.Count > 0)
                return true;
            else
                return false;
        }

        public void StepBackwardsInChain()
        {
            activeObject = activeObject._previousObjectInChain;
        }

        public void SetActiveObjectToSelectedImage(String filenameOfSelectedImage)
        {
            GetStartObject(filenameOfSelectedImage);
        }

        private void GetStartObject(String filename)
        {
            chainlink[0].HandleRequest(filename);
        }

        public CoR_Composite()
        {
        }

        private void AddObjectToChain(CoR_Object lastObjectInChain)
        {
            chainlink.Add(lastObjectInChain);
        }

        public CoR_Object _activeObject
        {
            get { return activeObject; }
            set { activeObject = value; }
        }

        public int _indexForSelectedObject
        {
            get { return indexForSelectedObject; }
            set { indexForSelectedObject = value; }
        }

        public int GetObjectCount()
        {
            return chainlink.Count();
        }

        public void ClearObject()
        {
            chainlink.Clear();
            activeObject = null;
        }
    }
}
