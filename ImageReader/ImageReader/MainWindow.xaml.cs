﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace ImageReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int MAX_ZOOMLEVEL = 10;
        const int MIN_ZOOMLEVEL = 1;

        private BitmapImage loadedPictureASBitmap = new BitmapImage();
        private int imageRotation = 0;
        private String[] pictures;
        private CoR_Composite chain;

        public MainWindow()
        {
            InitializeComponent();
            chain = new CoR_Composite();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            string selectedFileName = OpenDialog();
            LoadImageOntoScreen(selectedFileName);
        }

        private void LoadImageOntoScreen(String file)
        {
            if (file == "NoFileSelectedError")
            {
                Console.WriteLine("No File Selected Error!");
            }
            else if (file == "FileIsNotAnImage")
            {
                Console.WriteLine("Selected file is not an image!");
            }
            else
            {
                Uri uriFromFileName = new System.Uri(file);
                loadedPictureASBitmap = BitmapFromUri(uriFromFileName);
                shownImage.Source = loadedPictureASBitmap;
                LoadFileNameIntoArray(file);
                LoadFilesIntoChain();
            }
        }
        private void LoadFileNameIntoArray(String path)
        {
            String openedFile = path;
            String openedDirectory = System.IO.Path.GetDirectoryName(openedFile);
            pictures = Directory.GetFiles(openedDirectory, "*.*");
        }

        private void LoadFilesIntoChain()
        {
            foreach (String filename in pictures)
            {
                if (IsFileAnImage(filename))
                    chain.CreateNewObjectInChain(filename);
            }

        }

        private bool IsFileAnImage(String filename)
        {
            String fileExtension = filename.Substring(filename.LastIndexOf('.'));
            String fileType = fileExtension.ToLower();

            if (fileType == ".jpg" || fileType == "bmp" || fileType == ".gif" || fileType == ".png")
                return true;
            else
                return false;
        }

        private BitmapImage BitmapFromUri(Uri source)
        {
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = source;
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.EndInit();
            return bitmap;
        }

        private RenderTargetBitmap GetImageFromGUI()
        {
            Transform transform = shownImage.LayoutTransform;
            shownImage.LayoutTransform = null;
            Size size = new Size(shownImage.ActualWidth, shownImage.ActualHeight);
            shownImage.Measure(size);
            shownImage.Arrange(new Rect(size));

            RenderTargetBitmap renderBitmap = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96d, 96d, PixelFormats.Default);
            renderBitmap.Render(shownImage);

            return renderBitmap;
        }

        private BitmapEncoder GetEncoderFrom(String filename)
        {
            BitmapEncoder encoder = new BmpBitmapEncoder();
            String fileExtension = filename.Substring(filename.LastIndexOf('.'));
            switch (fileExtension.ToLower())
            {
                case ".jpg":
                    encoder = new JpegBitmapEncoder();
                    break;
                case ".bmp":
                    encoder = new BmpBitmapEncoder();
                    break;
                case ".gif":
                    encoder = new GifBitmapEncoder();
                    break;
                case ".png":
                    encoder = new PngBitmapEncoder();
                    break;
            }

            return encoder;
        }

        private void Rotate_Left_Click(object sender, RoutedEventArgs e)
        {
            imageRotation -= 90;
            RotateBitmapImage();
            Save_Click(sender, e);
        }

        private void Rotate_Right_Click(object sender, RoutedEventArgs e)
        {

            imageRotation = imageRotation + 90;
            RotateBitmapImage();
            Save_Click(sender, e);
        }

        private void Zoomcontroll_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int zoomfactor = 1;
            zoomfactor = (int)e.NewValue;
        }

        private void ZoomIn_Click(object sender, RoutedEventArgs e)
        {
            if (!IsZoomLevelAtMaxLevel())
            {
                ScaleBitmapImage("ZOOM_IN");
                Zoomcontroll.Value++;
            }
            else
                Console.WriteLine("Max ZoomLevel Reached");
        }

        private void ZoomOut_Click(object sender, RoutedEventArgs e)
        {
            if (!IsZoomLevelAtMinLevel())
            {
                ScaleBitmapImage("ZOOM_OUT");
                Zoomcontroll.Value--;
            }
            else
                Console.WriteLine("Min ZoomLevel Reached");
        }

        private bool IsZoomLevelAtMaxLevel()
        {
            if (Zoomcontroll.Value == MAX_ZOOMLEVEL)
                return true;
            else return false;
        }

        private bool IsZoomLevelAtMinLevel()
        {
            if (Zoomcontroll.Value == MIN_ZOOMLEVEL)
                return true;
            else return false;
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        private void ScaleBitmapImage(String zoom)
        {
            double width = loadedPictureASBitmap.Width;
            double height = loadedPictureASBitmap.Height;
            Matrix m = shownImage.RenderTransform.Value;

            if (zoom.Equals("ZOOM_IN"))
                m.ScaleAtPrepend(1.1 / 1, 1.1 / 1, width / 2, height / 2);
            else
                m.ScaleAtPrepend(1 / 1.1, 1 / 1.1, width / 2, height / 2);

            shownImage.RenderTransform = new MatrixTransform(m);
        }

        private void RotateBitmapImage()
        {
            TransformedBitmap tempImage = new TransformedBitmap();

            tempImage.BeginInit();
            tempImage.Source = loadedPictureASBitmap; // MyImageSource of type BitmapImage

            RotateTransform transform = new RotateTransform(imageRotation);
            tempImage.Transform = transform;
            tempImage.EndInit();

            shownImage.Source = tempImage;
        }

        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            if (chain.ImagesInChain())
            {
                chain.StepBackwardsInChain();
                LoadImageOntoScreen(chain._activeObject._name);
                imageRotation = 0;
            }
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {

            if (chain.ImagesInChain())
            {
                chain.StepForwardInChain();
                LoadImageOntoScreen(chain._activeObject._name);
                imageRotation = 0;
            }
        }

        private void EmptyChain()
        {
            chain.ClearObject();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Close();
        }

        private void SaveAs_Click(object sender, RoutedEventArgs e)
        {
            String filename = SaveasDialog();
            RenderTargetBitmap bitmapImage = GetImageFromGUI();
            loadedPictureASBitmap = null;

            using (FileStream filestream = File.Open(filename, FileMode.Create))
            {
                BitmapEncoder encoder = GetEncoderFrom(filename);
                encoder.Frames.Add(BitmapFrame.Create(bitmapImage));

                encoder.Save(filestream);
                filestream.Close();
            }
        }

        private String OpenDialog()
        {
            Microsoft.Win32.OpenFileDialog open = new Microsoft.Win32.OpenFileDialog();
            open.Filter = "All Files|*.*|JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif|Png Image|*.png";
            open.ShowDialog();
            if (IsFileAnImage(open.FileName))
            {
                if (File.Exists(open.FileName))
                {
                    chain.ClearObject();
                    return open.FileName;
                }
                else
                    return "NoFileSelectedError";
            }
            else
                return "FileIsNotAnImage";
        }

        private String SaveasDialog()
        {
            Microsoft.Win32.SaveFileDialog savefile = new Microsoft.Win32.SaveFileDialog();
            savefile.DefaultExt = ".jpg";
            savefile.DefaultExt = ".bmp";
            savefile.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif|Png Image|*.png";

            savefile.ShowDialog();

            return savefile.FileName;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            RenderTargetBitmap bitmapImage = GetImageFromGUI();
            String filename = chain._activeObject._name;

            using (FileStream filestream = File.Open(filename, FileMode.Create))
            {
                BitmapEncoder encoder = GetEncoderFrom(filename);
                encoder.Frames.Add(BitmapFrame.Create(bitmapImage));

                encoder.Save(filestream);
                filestream.Close();
            }
        }

    }
}
