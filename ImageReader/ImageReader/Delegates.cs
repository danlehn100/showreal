﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader
{
    public delegate void CoR_Delegate(CoR_Object selectedObject);

    interface Delegates
    {
        event CoR_Delegate SelectedObject;
    }
}
