﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Data.SqlClient;
using unirest_net;
using Microsoft.Research.DynamicDataDisplay;
using System.Collections.ObjectModel;
using Microsoft.Research.DynamicDataDisplay.DataSources;

namespace External_API
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<City> weatherdata = new List<City>();
        SMHI_JSON weatherInformation;

        public MainWindow()
        {
            InitializeComponent();
            AddCitysTooList();
        }

        private void AddCitysTooList()
        {
            weatherdata.Add(new City("Karlstad", 59.4021806F, 13.511497700000064F));
            weatherdata.Add(new City("Jönköping", 57.78261370000001F, 14.161787600000025F));

            foreach (City city in weatherdata)
               Citys.Items.Add(city._name);
        }

        private void search_Click(object sender, RoutedEventArgs e)
        {
            String completeURL = AssembleURL(searchForThisTitle.Text);
            foundTitles.Items.Clear();
            SearchManyBooks(completeURL);
        }     

        private void foundTitles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String selectedTitle = foundTitles.SelectedItem.ToString();
            String urlTooBook = AssembleURL(selectedTitle);
            Item selectedBook = SearchOneBook(urlTooBook);
            DisplayBook(selectedBook);
        }

        private void foundCards_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DisplayBook(Item selectedBook)
        {
            Item chosenBook = selectedBook;
            ImageSource imageSource = new BitmapImage(new Uri(chosenBook.volumeInfo.imageLinks.smallThumbnail));
            selectedTitle.Text = chosenBook.volumeInfo.title;
            selectedSubTitle.Text = chosenBook.volumeInfo.subtitle;
            description.Text = chosenBook.volumeInfo.description;
            smallThumbnail.Source = imageSource;
        }

        private String AssembleURL(String bookTitle)
        {
            String url = "https://www.googleapis.com/books/v1/volumes?q=" + bookTitle;
            url = url.Replace(" ", "+");
            return url;
        }

     
        private void SearchManyBooks(String url)
        {
            String urlGoogle = url;
            var googleJSON = GET(urlGoogle);

            RootObject shelfOfBooks = JsonConvert.DeserializeObject<RootObject>(googleJSON);

            if (shelfOfBooks.totalItems > 0)
            {
                for (int i = 0; i < shelfOfBooks.items.Count; i++)
                {
                    foundTitles.Items.Add(shelfOfBooks.items[i].volumeInfo.title);
                }
            }
        }

        private Item SearchOneBook(String url)
        {
            String urlGoogle = url;
            var googleJSON = GET(urlGoogle);

            RootObject shelfOfBooks = JsonConvert.DeserializeObject<RootObject>(googleJSON);
            return shelfOfBooks.items[0];
        }


        public String GET(String url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }

        private void foundCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedIndex = Citys.SelectedIndex;
            Citylabel.Content = Citys.SelectedValue;
            String selectedCity = Citys.SelectedItem.ToString();
            float latitude = weatherdata[selectedIndex]._latitud;
            float longitud = weatherdata[selectedIndex]._longitud;
            
            String urlTooCity = AssembleSearchURL(latitude, longitud);
            String rawdataFromAPI = GET(urlTooCity);
            weatherInformation = ConvertSMHIStringTooJSON(rawdataFromAPI);
            AddDates(weatherInformation);
        }

        private void AddDates(SMHI_JSON data)
        {
            SMHI_JSON dataAsJSON = data;
            String currantdate = "";
            String previousdate = "";
            foreach (Timesery timeevent in data.timeseries)
            {
                currantdate = timeevent.validTime;
                currantdate = currantdate.Substring(0, 10);
                if (currantdate != previousdate)
                {
                    Dates.Items.Add(currantdate);
                    previousdate = currantdate;
                }
            }      
        }

        private void Dates_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Times.Items.Clear();
            String selectedDate = Dates.SelectedValue.ToString();
            String currantdate = "";
            foreach (Timesery timeevent in weatherInformation.timeseries)
            {
                currantdate = timeevent.validTime;
                currantdate = currantdate.Substring(0, 10);

                if (selectedDate == currantdate)
                {
                    String time = timeevent.validTime;
                    time = time.Substring(11, 8);
                    Times.Items.Add(time);
                }
            }
        }

        private void Time_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Do data search
            if (Times.Items.Count > 0)
            {
                String selectedDate = Dates.SelectedValue.ToString();
                String selectedTime = Times.SelectedValue.ToString();
                String datetimeKey = ReassembelDateTime(selectedDate, selectedTime);
                Timesery weatherinformation = GetDataFromCityBasedOnSelectedTimeAndDate(datetimeKey);

                Time.Content = selectedTime;
                Temperatur.Content = weatherinformation.t;
                Windspeed.Content = weatherinformation.ws;
                Humidity.Content = weatherinformation.r;    }          
        } 

        private Timesery GetDataFromCityBasedOnSelectedTimeAndDate(String datatimeKey)
        {
            foreach (Timesery timeevent in weatherInformation.timeseries)
            {
                if (timeevent.validTime == datatimeKey)
                    return timeevent;
            }
            return null;
        }
        private String ReassembelDateTime (String date, String time)
        {
            String assembeldDateTime = "";
            assembeldDateTime = date + "T" + time + "Z";            
            return assembeldDateTime;
        }

        private SMHI_JSON ConvertSMHIStringTooJSON(String dataAsString)
        {
            String rawDataFromSMHI = dataAsString;

            SMHI_JSON dataAsJSON = JsonConvert.DeserializeObject<SMHI_JSON>(rawDataFromSMHI);
            return dataAsJSON;                       
        }

        private String AssembleSearchURL(float latitude, float longitud)
        {
            String baseURL = "http://opendata-download-metfcst.smhi.se/api/category/pmp1.5g/version/1/geopoint/";
            String latitudeURL = "lat/" + latitude.ToString();
            String longitudURL = "/lon/" + longitud.ToString();
            String endURL = "/data.json"; 
            
            latitudeURL = latitudeURL.Replace(",", ".");            
            longitudURL = longitudURL.Replace(",", ".");
            
            String completeURL = baseURL + latitudeURL + longitudURL + endURL;

            return completeURL;
        }
 
    }

    public class IndustryIdentifier
    {
        public string type { get; set; }
        public string identifier { get; set; }
    }

    public class ReadingModes
    {
        public bool text { get; set; }
        public bool image { get; set; }
    }

    public class ImageLinks
    {
        public string smallThumbnail { get; set; }
        public string thumbnail { get; set; }
    }

    public class VolumeInfo
    {
        public string title { get; set; }
        public List<string> authors { get; set; }
        public string publishedDate { get; set; }
        public List<IndustryIdentifier> industryIdentifiers { get; set; }
        public ReadingModes readingModes { get; set; }
        public int pageCount { get; set; }
        public string printType { get; set; }
        public string maturityRating { get; set; }
        public bool allowAnonLogging { get; set; }
        public string contentVersion { get; set; }
        public ImageLinks imageLinks { get; set; }
        public string language { get; set; }
        public string previewLink { get; set; }
        public string infoLink { get; set; }
        public string canonicalVolumeLink { get; set; }
        public string publisher { get; set; }
        public string description { get; set; }
        public List<string> categories { get; set; }
        public double? averageRating { get; set; }
        public int? ratingsCount { get; set; }
        public string subtitle { get; set; }
    }

    public class ListPrice
    {
        public double amount { get; set; }
        public string currencyCode { get; set; }
    }

    public class RetailPrice
    {
        public double amount { get; set; }
        public string currencyCode { get; set; }
    }

    public class ListPrice2
    {
        public double amountInMicros { get; set; }
        public string currencyCode { get; set; }
    }

    public class RetailPrice2
    {
        public double amountInMicros { get; set; }
        public string currencyCode { get; set; }
    }

    public class Offer
    {
        public int finskyOfferType { get; set; }
        public ListPrice2 listPrice { get; set; }
        public RetailPrice2 retailPrice { get; set; }
    }

    public class SaleInfo
    {
        public string country { get; set; }
        public string saleability { get; set; }
        public bool isEbook { get; set; }
        public ListPrice listPrice { get; set; }
        public RetailPrice retailPrice { get; set; }
        public string buyLink { get; set; }
        public List<Offer> offers { get; set; }
    }

    public class Epub
    {
        public bool isAvailable { get; set; }
        public string acsTokenLink { get; set; }
    }

    public class Pdf
    {
        public bool isAvailable { get; set; }
        public string acsTokenLink { get; set; }
    }

    public class AccessInfo
    {
        public string country { get; set; }
        public string viewability { get; set; }
        public bool embeddable { get; set; }
        public bool publicDomain { get; set; }
        public string textToSpeechPermission { get; set; }
        public Epub epub { get; set; }
        public Pdf pdf { get; set; }
        public string webReaderLink { get; set; }
        public string accessViewStatus { get; set; }
        public bool quoteSharingAllowed { get; set; }
    }

    public class SearchInfo
    {
        public string textSnippet { get; set; }
    }

    public class Item
    {
        public string kind { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public string selfLink { get; set; }
        public VolumeInfo volumeInfo { get; set; }
        public SaleInfo saleInfo { get; set; }
        public AccessInfo accessInfo { get; set; }
        public SearchInfo searchInfo { get; set; }
    }

    public class RootObject
    {
        public string kind { get; set; }
        public int totalItems { get; set; }
        public List<Item> items { get; set; }
    }

    public class Mechanic
    {
        public string name { get; set; }
    }

    public class RootObjectHeartstone
    {
        public string cardId { get; set; }
        public string name { get; set; }
        public string cardSet { get; set; }
        public string type { get; set; }
        public string text { get; set; }
        public string locale { get; set; }
        public string faction { get; set; }
        public string rarity { get; set; }
        public int? cost { get; set; }
        public int? attack { get; set; }
        public int? health { get; set; }
        public string flavor { get; set; }
        public string artist { get; set; }
        public bool? collectible { get; set; }
        public string race { get; set; }
        public string playerClass { get; set; }
        public string howToGet { get; set; }
        public string howToGetGold { get; set; }
        public string img { get; set; }
        public string imgGold { get; set; }
        public List<Mechanic> mechanics { get; set; }
        public bool? elite { get; set; }
    }
}

