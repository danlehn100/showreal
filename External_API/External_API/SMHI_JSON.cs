﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace External_API
{
    public class SMHI_JSON
    {
        public double lat { get; set; }
        public double lon { get; set; }
        public string referenceTime { get; set; }
        public List<Timesery> timeseries { get; set; }
    }

    public class Timesery
    {
        public string validTime { get; set; }
        public double t { get; set; }
        public int tcc { get; set; }
        public int lcc { get; set; }
        public int mcc { get; set; }
        public int hcc { get; set; }
        public int tstm { get; set; }
        public int r { get; set; }
        public double vis { get; set; }
        public double gust { get; set; }
        public double pit { get; set; }
        public double pis { get; set; }
        public int pcat { get; set; }
        public double msl { get; set; }
        public int wd { get; set; }
        public double ws { get; set; }
    }
    
}
