﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace External_API
{
    public class City
    {
        private String name;
        private float latitud;
        private float longitud;

        public City (String city, float x, float y)
        {
            this._name = city;
            this._latitud = x;
            this._longitud = y;
        }

        public string _name
        {
            get { return name; }
            set { name = value; }
        }

        public float _latitud
        {
            get { return latitud; }
            set { latitud = value; }
        }

        public float _longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }
    }


}
