﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Win32;
using System.Windows.Media;

namespace NoteMaker
{
    public class Settings
    {
        private string settingsFile = "Settings.ini";
        private string loadPath;
        private string savePath;

        private List<HeadlineStandard> fontShortcuts = new List<HeadlineStandard>();

        public Settings()
        {
            loadPath = "";
            savePath = "";
            SetHeadlineStandard();
        }

        public HeadlineStandard GetHeadlineFont(int index)
        {
            return fontShortcuts[index];
        }

        public List<HeadlineStandard> _fontShortcuts
        {
            get { return fontShortcuts; }
            set { fontShortcuts = value; }
        }

        public string _loadPath
        {
            get { return loadPath; }
            set { loadPath = value; }
        }

        public string _savePath
        {
            get { return savePath; }
            set { savePath = value; }
        }

        private void Load()
        {
            Settings temporarySettingsObject = ReadFromBinaryFile(settingsFile);
            this._loadPath = temporarySettingsObject._loadPath;
            this._savePath = temporarySettingsObject._savePath;
        }

        public void WriteToBinaryFile(string filePath, Settings objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        public Settings ReadFromBinaryFile(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (Settings)binaryFormatter.Deserialize(stream);
            }
        }

        private void Save()
        {
            WriteToBinaryFile(settingsFile, this, false);
        }

        private void SetHeadlineStandard()
        {
            Console.WriteLine("Adding standard Fonts");
            fontShortcuts.Add(new HeadlineStandard("H1", "Times New Roman", "24"));
            fontShortcuts.Add(new HeadlineStandard("H2", "Times New Roman", "16"));
            fontShortcuts.Add(new HeadlineStandard("H3", "Times New Roman", "12"));
            Console.Write("There are fontscount: {0}", fontShortcuts.Count);
        }

        public void SetHeadlineStandard(int buttonID, String name, String family, String size)
        {
            fontShortcuts[buttonID]._name = name;
            fontShortcuts[buttonID]._family = new FontFamily(family);
            fontShortcuts[buttonID]._size = size;

            Console.WriteLine("Font {0} has been changed.", buttonID + 1);
        }
    }

    public class HeadlineStandard
    {
        private String name;
        private FontFamily family;
        private String size;

        public HeadlineStandard(String buttonName, String fontName, String fontSize)
        {
            name = buttonName;
            family = new FontFamily(fontName);
            size = fontSize;
        }

        public HeadlineStandard()
        {
            name = "H?";
            family = new FontFamily("Times New Roman");
            size = "12";
        }

        public String _name
        {
            set { name = value; }
            get { return name; }
        }

        public FontFamily _family
        {
            set { family = value; }
            get { return family; }
        }

        public String _size
        {
            set { size = value; }
            get { return size; }
        }

    }
}
