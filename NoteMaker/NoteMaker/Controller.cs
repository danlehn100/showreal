﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteMaker
{
    public class Controller
    {
        MainWindow view;
        Model model;

        public Controller(MainWindow linkToView)
        {
            view = linkToView;
            model = new Model();
            AddDelegates();
            // model.LoadSettings();
        }

        public HeadlineStandard GetStandardFont(int fontIndex)
        {
            return (model._programSettings.GetHeadlineFont(fontIndex));
        }

        private void AddDelegates()
        {
            model.ChangeLoadPath += new Delegate_TabPath(this.view.UpdateLoadPath);
            model.ChangeSavePath += new Delegate_TabPath(this.view.UpdateSavePath);
            // model.LoadIntoProgram += new Delegate_Settings(this.view.LoadStartupSettings);
        }

        public void SetLoadPath(string directoryPath)
        {
            model.ChangeSettings("Load", directoryPath);
        }

        public void SetSavePath(String directoryPath)
        {
            model.ChangeSettings("Save", directoryPath);
        }

        public void Save_Executed()
        {
            if (model._activeFile == "")
                view.SaveFileAs();
            else
                view.SaveFile(model._activeFile);
        }

        public void ActiveFileChanged(String fileName)
        {
            model._activeFile = fileName;
        }

        public void SetHeadlineStandard(int buttonID, String name, String family, String size)
        {
            model.SetHeadlineStandard(0, name, family, size);
        }
    }
}
