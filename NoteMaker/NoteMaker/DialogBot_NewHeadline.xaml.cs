﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NoteMaker
{
    /// <summary>
    /// Interaction logic for HeadlineSettings.xaml
    /// </summary>
    public partial class HeadlineSettings : Window
    {
        private String fontName;
        private String fontFamily;
        private String fontSize;

        public HeadlineSettings()
        {
            InitializeComponent();
            cmbFontFamily.ItemsSource = Fonts.SystemFontFamilies.OrderBy(f => f.Source);
            cmbFontSize.ItemsSource = new List<double>() { 8, 10, 12, 14, 18, 24, 32, 48, 72 };
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {

            _fontName = name.Text;
            _fontFamily = cmbFontFamily.SelectedItem.ToString();
            _fontSize = cmbFontSize.SelectedItem.ToString();

            this.Close();
        }

        public void ChangeHeadLine(HeadlineSettings newHeadline)
        {
            this._fontName = newHeadline._fontName;
            this._fontFamily = newHeadline._fontFamily;
            this._fontSize = newHeadline._fontSize;
        }

        public String _fontName
        {
            set { fontName = value; }
            get { return fontName; }
        }

        public String _fontFamily
        {
            set { fontFamily = value; }
            get { return fontFamily; }
        }

        public String _fontSize
        {
            set { fontSize = value; }
            get { return fontSize; }
        }
    }
}
