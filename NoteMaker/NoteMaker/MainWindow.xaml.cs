﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using System.Windows.Markup;

namespace NoteMaker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Controller controller;
        private FontFamily h1 = new FontFamily("Times New Roman");

        public MainWindow()
        {
            InitializeComponent();
            StartMVC();
            cmbFontFamily.ItemsSource = Fonts.SystemFontFamilies.OrderBy(f => f.Source);
            cmbFontSize.ItemsSource = new List<double>() { 8, 10, 12, 14, 18, 24, 32, 48, 72 };


            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        private void StartMVC()
        {
            controller = new Controller(this);
        }


        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            string nameOfSelectedFile = GetFileTooOpen();
            LoadDocument(nameOfSelectedFile);
        }

        private String GetFileTooOpen()
        {
            string nameOfSelectedFile = "";

            OpenFileDialog openFileDialog = new OpenFileDialog();
            //  openFileDialog.DefaultExt = "*.rtf;
            // openFileDialog.Filter = "RTF Files*.rtf|TXT Files|*.txt";
            openFileDialog.InitialDirectory = loadPath.Text;

            if (openFileDialog.ShowDialog() == true)
                nameOfSelectedFile = openFileDialog.FileName;


            return nameOfSelectedFile;
        }

        private void LoadDocument(String fileName)
        {
            String fileType = System.IO.Path.GetExtension(fileName);
            if (fileType == ".rtf" || fileType == ".RTF")
            {
                String readText = File.ReadAllText(fileName);
                Paragraph textAsParagraph = new Paragraph();
                textAsParagraph.Inlines.Add(new Run(readText));

                MemoryStream stream = new MemoryStream(ASCIIEncoding.Default.GetBytes(readText));
                this.textContent.Selection.Load(stream, DataFormats.Rtf);
                stream.Close();
            }
            else
            {
                String readText = File.ReadAllText(fileName);
                Paragraph textAsParagraph = new Paragraph();
                textAsParagraph.Inlines.Add(new Run(readText));
                FlowDocument flow = new FlowDocument();
                flow.Blocks.Add(textAsParagraph);
                textContent.Document = flow;
            }
        }

        private void New_Executed(object sender, ExecutedRoutedEventArgs e)
        {

            if (textContent.Document.Blocks.Count != 0)
            {
                MessageBoxResult anwser = MessageBox.Show("Save file before clearing text?", "Confirmation", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                if (anwser == MessageBoxResult.Yes)
                {
                    Save_Executed(this, null);
                    textContent.Document.Blocks.Clear();
                }
                else if (anwser == MessageBoxResult.No)
                    textContent.Document.Blocks.Clear();
            }

        }

        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (textContent.Document.Blocks.Count != 0)
            {
                MessageBoxResult anwser = MessageBox.Show("Save file before loading?", "Confirmation", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                if (anwser == MessageBoxResult.Yes)
                {
                    Save_Executed(this, null);
                }
                else if (anwser == MessageBoxResult.No)
                {
                    string nameOfSelectedFile = GetFileTooOpen();
                    LoadDocument(nameOfSelectedFile);
                }
            }
        }

        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            controller.Save_Executed();
        }

        public void SaveFile(String fileName)
        {
            SaveAsRTL(fileName);
        }

        private void SaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileAs();
        }

        public void SaveFileAs()
        {
            string nameOfSelectedFile = GetFileTooSave();
            if (nameOfSelectedFile != null)
            {
                SaveAsRTL(nameOfSelectedFile);
                controller.ActiveFileChanged(nameOfSelectedFile);
            }
        }

        private String GetFileTooSave()
        {
            string nameOfSelectedFile = "";

            SaveFileDialog fileNameDialog = new SaveFileDialog();
            fileNameDialog.DefaultExt = "*.rtf";
            fileNameDialog.Filter = "RTF Files|*.rtf";

            if (fileNameDialog.ShowDialog() == true)
            {
                nameOfSelectedFile = fileNameDialog.FileName;
                return nameOfSelectedFile;
            }
            else return null;
        }

        private void SaveAsRTL(String fileToSave)
        {
            FileStream fileStream = new FileStream(fileToSave, FileMode.Create);
            TextRange range = new TextRange(textContent.Document.ContentStart, textContent.Document.ContentEnd);
            range.Save(fileStream, DataFormats.Rtf);
            fileStream.Close();
        }


        // Link between Model and View
        public void UpdateLoadPath(String newLoadPath)
        {
            loadPath.Text = newLoadPath;
        }

        public void UpdateSavePath(String newSavePath)
        {
            Console.WriteLine("Path is {0}", newSavePath);
            savePath.Text = newSavePath;
        }

        // Button Event Method
        private void changeLoadPath_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "All Files (*.*)|*.*";
            fileDialog.FilterIndex = 1;
            fileDialog.Multiselect = true;

            if (fileDialog.ShowDialog() == true)
                controller.SetLoadPath(System.IO.Path.GetDirectoryName(fileDialog.FileName));

        }

        private void changeSavePath_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "All Files (*.*)|*.*";
            fileDialog.FilterIndex = 1;
            fileDialog.Multiselect = true;

            if (fileDialog.ShowDialog() == true)
                controller.SetSavePath(System.IO.Path.GetDirectoryName(fileDialog.FileName));
        }

        private void textContent_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }


        private void cmbFontFamily_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbFontFamily.SelectedItem != null)
                textContent.Selection.ApplyPropertyValue(Inline.FontFamilyProperty, cmbFontFamily.SelectedItem);
        }

        private void cmbFontFamily_SelectionChangedHeadLineOne(object sender, SelectionChangedEventArgs e)
        {
            textContent.Selection.ApplyPropertyValue(Inline.FontFamilyProperty, new FontFamily("Ariel"));
        }


        private void cmbFontSize_TextChanged(object sender, TextChangedEventArgs e)
        {
            textContent.Selection.ApplyPropertyValue(Inline.FontSizeProperty, cmbFontSize.Text);
        }


        private void HeadlineOne_Click(object sender, RoutedEventArgs e)
        {
            HeadlineStandard standardFontStoredInModel;
            standardFontStoredInModel = controller.GetStandardFont(0);
            textContent.Selection.ApplyPropertyValue(Inline.FontFamilyProperty, standardFontStoredInModel._family);
            textContent.Selection.ApplyPropertyValue(Inline.FontSizeProperty, (object)standardFontStoredInModel._size);
        }

        private void HeadlineTwo_Click(object sender, RoutedEventArgs e)
        {
            String sizeOfText;
            sizeOfText = "30";
            textContent.Selection.ApplyPropertyValue(Inline.FontFamilyProperty, new FontFamily("Times New Roman"));
            textContent.Selection.ApplyPropertyValue(Inline.FontSizeProperty, (object)sizeOfText);
        }

        private void HeadlineThree_Click(object sender, RoutedEventArgs e)
        {
            String sizeOfText;
            sizeOfText = "20";
            textContent.Selection.ApplyPropertyValue(Inline.FontFamilyProperty, new FontFamily("Times New Roman"));
            textContent.Selection.ApplyPropertyValue(Inline.FontSizeProperty, (object)sizeOfText);
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Close();
        }

        private void headlineOneSetting_Click(object sender, RoutedEventArgs e)
        {
            String name = "";
            String family = "";
            String size = "";

            Console.WriteLine("Name: {0}, Family: {1}, Size: {2}", name, family, size);
            HeadlineSettings dialogBox = new HeadlineSettings();

            if (dialogBox.ShowDialog() == false)
            {
                name = dialogBox._fontName;
                family = dialogBox._fontFamily;
                size = dialogBox._fontSize;
            }

            Console.WriteLine("Name: {0}, Family: {1}, Size: {2}", name, family, size);
            controller.SetHeadlineStandard(0, name, family, size);
        }

    }


}
