﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteMaker
{
    public delegate void Delegate_TabPath(string directoryPath);

    interface iDelegates
    {
        event Delegate_TabPath ChangeLoadPath;
        event Delegate_TabPath ChangeSavePath;
        event Delegate_TabPath ChangeActiveFile;
    }

    public delegate void Delegate_Settings(Settings userSettings);

    interface iDelegate_Settings
    {
        event Delegate_Settings LoadIntoProgram;
    }
}

