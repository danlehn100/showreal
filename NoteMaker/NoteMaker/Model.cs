﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteMaker
{
    public class Model : iDelegates
    {
        private Settings programSettings;
        private String activeFile;
        private Boolean changed = false;
        //public List<HeadlineStandard> fontShortcuts = new List<HeadlineStandard>(); 

        public event Delegate_TabPath ChangeLoadPath;
        public event Delegate_TabPath ChangeSavePath;
        public event Delegate_TabPath ChangeActiveFile;

        //  public event Delegate_Settings LoadIntoProgram;


        public Model()
        {
            programSettings = new Settings();
            activeFile = "";
        }



        public void ChangeSettings(string fileMode, string directoryPath)
        {
            switch (fileMode)
            {
                case "Load":
                    _programSettings._loadPath = directoryPath;
                    ChangeLoadPath.Invoke(_programSettings._loadPath);
                    break;

                case "Save":
                    _programSettings._savePath = directoryPath;
                    ChangeSavePath.Invoke(_programSettings._savePath);
                    break;
            }
        }
        /*
        public void LoadSettings ()
        {
            LoadIntoProgram.Invoke(programSettings);
        }
        */

        public Settings _programSettings
        {
            get { return programSettings; }
            set { programSettings = value; }
        }

        public String _activeFile
        {
            get { return activeFile; }
            set { activeFile = value; }
        }

        public void SetHeadlineStandard(int buttonID, String name, String family, String size)
        {
            programSettings.SetHeadlineStandard(buttonID, name, family, size);
        }
    }


}
